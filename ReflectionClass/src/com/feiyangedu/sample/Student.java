package com.feiyangedu.sample;

public class Student extends Person {
	private String id;

	// 
//	public Student (String name, String id) {
//		super();
//		this.name = name;
//		this.id = id;
//	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public void hello() {
		System.out.println("Hello " + name + ",id: " + id);
	}
}
