package com.feiyangedu.sample;
public class Main {
	public static void main(String[] args) throws Exception  {
		// 泛型
		Class cls;
		if (false) {
			cls = Class.forName("com.feiyangedu.sample.Student");
		} else {
			cls = Class.forName("com.feiyangedu.sample.Person");
		}
		
		System.out.println("class name: " + cls.getName());
		System.out.println("class simple name: " + cls.getSimpleName());
		System.out.println("package name: " + cls.getPackage().getName());
		System.out.println("is interface?: " + cls.isInterface());
		
		// 不支持传递参数
		Person s = (Person) cls.newInstance();
		s.hello();
	}
}